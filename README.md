# This is the readme for Monitor.Me()

##Pre-configuration required

1.Graphite 0.9.10-All dependencies met

2.Collectd 5.3.0 -currently Monitor.Me is configured to show the following graphs from collectd.make sure these are uncommented in collectd.conf
1.contextswitch
2.cpu
3.cpufreq
4.load
5.memory
6.irq
7.disk
8.processes
9.interface
10.uptime
11.Vmem
12.writegraphite
13.rrdtool
14.rrdcached

3.Make sure rrdtool-python is installed

4.To view munin files create a sysmlink of the munin /var/lib/munin/yourmuninname to

#Configuration for Monitor.Me();
##List of folders and files-their names and usage

##Css-consists of all css files required
1.bootstrap.css/bootstrap.min.css 
-main css file
##Files mainly for graphing purpose
3.main.css 
4.graph.css
5.detail.css
6.legend.css
##Js-
consists of javascript files required
##Font-
fonts used in the html pages.

#Main files

1. ###Dashboard.html-
This is the main web based dashboard page.

2. ###db.php/my.php -
These files contain the backend scripting.db.php is the web service written for kpoint db.

3. ###parseWatch.js-
 This file actually creates the Web based ui on the basis of the configured params directly.

4. ###extras.js
This file contains all the custom functions written to make monitor.me equivalent to kibana

5. ###/saved/
this directory contains all the dashboards that have been saved in json format.

#Configuration details
###1.config.js
Just one thing needs to be replaced
You shall find this on the first line.
##"var graphite url= "http://localhost";
replace this url by your graphite url.

##2.machine_name_munin=""
if you have munin configured on the system soft linked to /opt/graphite/storage/rrd
then to view those metrics mention the directory structure to the munin directory as 
shown in the graphite tree.
if not then just leave this blank

##2.kpoint_db=""
this takes in the parameters if you want to view db metrics on monitor.me
mention the correct username,password,host and database where you have the kpoint db.
if you dont want to view kpoint metrics just leave the field blank


#End of ReadMe 
