
var auth, changeDashboard, createGraph, dashboard, dataPoll, default_graphite_url, default_period, description, generateDataURL, generateEventsURL, generateGraphiteTargets, getTargetColor, graphScaffold, graphite_url, graphs, init, metrics, period, refresh, refreshSummary, refreshTimer, scheme, toggleCss, _avg, _formatBase1024KMGTP, _last, _max, _min, _sum, machine_name;

var num;

//values of globally accessible variables and init of some variables
graphite_url = graphite_url 
if(machine_name_rrd)
	machine_name = machine_name_rrd;
else if(machine_name_graphite)
	machine_name = machine_name_graphite;
else if(machine_name_munin)
	machine_name = machine_name_munin;
else
{
	//console.log("No machine_name specified");
	return;
}
default_graphite_url = graphite_url;
default_period = 1440;
if (scheme === void 0) {
	scheme = 'classic9';
}
period = default_period;
dashboard = dashboards[0];
metrics = dashboard['metrics'];
description = dashboard['description'];
refresh = dashboard['refresh'];
refreshTimer = null;
auth = auth != null ? auth : false;
graphs = [];

//utility function for computation
dataPoll = function() {
	//console.info("i am in data poll");
	var graph, _i, _len, _results;
	_results = [];
	for (_i = 0, _len = graphs.length; _i < _len; _i++) {
		graph = graphs[_i];
		_results.push(graph.refreshGraph(period));
	}
	return _results;
};

//fun-Sum
_sum = function(series) {
	return _.reduce(series, (function(memo, val) {
		return memo + val;
	}), 0);
};
//fun-Avg
_avg = function(series) {
	return _sum(series) / series.length;
};
//fun-Max
_max = function(series) {
	return _.reduce(series, (function(memo, val) {
		if (memo === null) {
			return val;
		}
		if (val > memo) {
			return val;
		}
		return memo;
	}), null);
};
//fun-Min
_min = function(series) {
	return _.reduce(series, (function(memo, val) {
		if (memo === null) {
			return val;
		}
		if (val < memo) {
			return val;
		}
		return memo;
	}), null);
};
//fun-Last
_last = function(series) {
	return _.reduce(series, (function(memo, val) {
		if (val !== null) {
			return val;
		}
		return memo;
	}), null);
};
//fun-Formatter function for axes data
_formatBase1024KMGTP = function(y, formatter) {
	var abs_y;
	if (formatter == null) {
		formatter = d3.format(".2r");
	}
	abs_y = Math.abs(y);
	if (abs_y >= 1125899906842624) {
		return formatter(y / 1125899906842624) + "P";
	} else if (abs_y >= 1099511627776) {
		return formatter(y / 1099511627776) + "T";
	} else if (abs_y >= 1073741824) {
		return formatter(y / 1073741824) + "G";
	} else if (abs_y >= 1048576) {
		return formatter(y / 1048576) + "M";
	} else if (abs_y >= 1024) {
		return formatter(y / 1024) + "K";
	} else if (abs_y < 1 && y > 0) {
		return formatter(y);
	} else if (abs_y === 0) {
		return 0;
	} else {
		return formatter(y);
	}
};
//fun-refreshSummary
refreshSummary = function(graph) {
	//console.info("i am in refreshSummary");
	var summary_func, y_data, _ref;
	if (!((_ref = graph.args) != null ? _ref.summary : void 0)) {
		return;
	}
	if (graph.args.summary === "sum") {
		summary_func = _sum;
	}
	if (graph.args.summary === "avg") {
		summary_func = _avg;
	}
	if (graph.args.summary === "min") {
		summary_func = _min;
	}
	if (graph.args.summary === "max") {
		summary_func = _max;
	}
	if (graph.args.summary === "last") {
		summary_func = _last;
	}
	if (typeof graph.args.summary === "function") {
		summary_func = graph.args.summary;
	}
	if (!summary_func) {
		console.log("unknown summary function " + graph.args.summary);
	}
	y_data = _.map(_.flatten(_.pluck(graph.graph.series, 'data')), function(d) {
		return d.y;
	});
	return $("" + graph.args.anchor + " .graph-summary").html(graph.args.summary_formatter(summary_func(y_data)));
};


//fun-graphScaffold-  render the main template widout the graphs
graphScaffold = function() 
{
	console.info("i am in graphScaffold");
	var colspan, context, converter, graph_template, i, metric, offset, _i, _len;
	graph_template = "{{#dashboard_description}}\n"
			+"<div class=\"well\">{{{dashboard_description}}}</div>\n"
			+"{{/dashboard_description}}\n{{#metrics}}\n "
			+"{{#start_row}}\n"
			+"<div class=\"row-fluid\" style=\"margin-top:30px;\">\n  {{/start_row}}\n "
			+"<div class=\"{{span}}\" id=\"graph-{{graph_id}}\" style=\"padding-left:23px;border:1.5px solid #ccc;outline: 1px solid lightslategray;background: whitesmoke;margin-top:20px;\" >\n "
			+"<h4 style=\"margin-top:0px;margin-bottom:15px;font-size:11px;margin-left:-16px;color:#666;\">{{metric_alias}}"
			+"<span class=\"pull-right \" style=\"margin-right:4px;margin-top:2px;\">"
			+"<a class=\"icon-plus-sign icon-large\" style=\"font-size:13px;color:#666\" title=\"Add new Graph\" onClick=\"$(\"row\").toggle\"></a>&nbsp&nbsp"
			+"<a class=\"icon-cog icon-large\" style=\"font-size:13px;color:#666\" data-reveal-id=\"Graph\" title=\"Configure Graph\" onClick=\"InitParams({{graph_id}})\"></a>&nbsp&nbsp"			
			+"<a class=\"icon-remove-sign icon-large\" style=\"font-size:13px;color:#666\" title=\"Remove graph\" id=\"close-{{graph_id}}\" data-id=\"{{graph_id}}\" onClick=\"removeGraph({{graph_id}})\" \"></a>"	
			+"</span></h4>\n"
			+"<div class=\"row\" style=\"margin-left:-2px;\">"
			+"<input id=\"input-{{graph_id}}\"type=\"text\" class=\"form-control\" placeholder=\"Add metrics to Graph\" style=\"padding-left:10px;border-radius:20px;width:500px\"/>"
			+"<a class=\"icon-plus\" style=\"font-size:18px;color:#666;margin-left:5px;\" title=\"Add to Graph\" onCLick=\"addToGraph({{graph_id}})\"></a>&nbsp&nbsp"		
			+"</div>"
			+"<div id=\"chart\" style=\"margin-top:20px;\"></div>\n"
			+"<div class=\"timeline\"></div>\n\n\n "
			+"<div id=\"graph-{{graph_id}}-slider\" style=\"margin-top:17px;\" ></div> "
			+"<h4 style=\"font-weight:bold;font-size:11px;margin-top:-15px;color:#666\"id=\"me-{{graph_id}}\">{{metric_description}}</h4>\n\n"
			+"<div class=\"legend\"></div>\n\n\n"
			+"</div>\n  {{#end_row}}\n  </div>\n  {{/end_row}}\n{{/metrics}}";
	$('#graphs').empty();
	context = {
		metrics: []
	};	
	converter = new Markdown.Converter();
	//create description html if found  
	if (description) {
		context['dashboard_description'] = converter.makeHtml(description);
	}
	offset = 0;
	//add the following to metrics array
	for (i = _i = 0, _len = metrics.length; _i < _len; i = ++_i) {
		metric = metrics[i];
		colspan = metric.colspan != null ? metric.colspan : 1;
		context['metrics'].push({
		start_row: offset % 3 === 0,
		end_row: offset % 3 === 2,
		graph_id: i,
		span: 'span' + (4 * colspan),
		metric_alias: metric.alias.toUpperCase(),
		metric_description: metric.description.toUpperCase()
		});
		offset += colspan;
	}
	//render the following info,create divs ,everything except the graphs
	return $('#graphs').append(Mustache.render(graph_template, context));
};


//fun-init  initialize the graph
init = function() {
	//console.info("i am in init");
	var dash, i, metric, refreshInterval, _i, _j, _len, _len1;
	//$('.dropdown-menu').empty();
	//find dashboards and init dropdown
	//for (_i = 0, _len = dashboards.length; _i < _len; _i++) {
	//	dash = dashboards[_i];
	//	$('.dropdown-menu').append("<li><a href=\"#\">" + dash.name + "</a></li>");
	//}
	//create the template along with static info
	initAutocomplete();
	graphScaffold();
	graphs = [];
	for (i = _j = 0, _len1 = metrics.length; _j < _len1; i = ++_j) {
		metric = metrics[i];
		//create each graph and push it into graphs div
		graphs.push(createGraph("#graph-" + i, metric,i));
	}	
//	$('.page-header h1').empty().append(dashboard.name);
	refreshInterval = refresh || 10000;
	autoComplete(i);
	if(metrics.length === 0){
		$('#graphs').append("<h4 style=\"font-weight:initial\"> There are no graphs configured on this dashboard. Click on Add graphs to create your custom dashboard.</h4>");
	}
	if (refreshTimer) {
		clearInterval(refreshTimer);
	}
	return refreshTimer = setInterval(dataPoll, refreshInterval);
};

//fun-getTargetColor
getTargetColor = function(targets, target) {
	//console.info("i am in getTargetColor");
	var t, _i, _len;
	if (typeof targets !== 'object') {
		return;
	}
	for (_i = 0, _len = targets.length; _i < _len; _i++) {
		t = targets[_i];
		if (!t.color) {
			continue;
		}
		if (machine_name + t.target === target || t.alias === target) {
			return t.color;
		}
	}
};


var saveDashboard=function(dname){
	$.ajax({
	url:"my.php",
	type:"post",
	data:{value:JSON.stringify(dashboards),name:dname,id:"Save"},   //Converts a JavaScript value to a JavaScript Object Notation (JSON) string.
	success:function(result){
		alert("Dashboard has been saved");
		console.log(result);
	}
});

};

var loadNames=function(dname){
	$.ajax({
	url:"my.php",
	type:"post",
	data:{value:JSON.stringify(dashboards),name:dname,id:"Load"},   //Converts a JavaScript value to a JavaScript Object Notation (JSON) string.
	success:function(result){
		alert("Dashboard has been loaded");
		console.log(result);
	}
});

};
//fun-generateGraphiteTargets   generate target string for each graph
generateGraphiteTargets = function(targets) {
	//console.info("i am in generateGraphiteTargets");
	var graphite_targets, target, _i, _len;
	if (typeof targets === "string") {
		return "&target=" /*+ machine_name */+ targets;
	}
	if (typeof targets === "function") {
		return "&target=" +/*+ machine_name */ + (targets());
	}
	graphite_targets = "";
	for (_i = 0, _len = targets.length; _i < _len; _i++) {
		target = targets[_i];
		if (typeof target === "string") {
			graphite_targets += "&target=" /*+ machine_name */+ target;
		}
		if (typeof target === "function") {
			graphite_targets += "&target=" /*+ machine_name */ + (target());
		}
		if (typeof target === "object") {
			graphite_targets += "&target=" /*+ machine_name */ + ((target != null ? target.target : void 0) || '');
		}
	}
	return graphite_targets;
};

//fun-generateDataURL  create data url like graphite part
generateDataURL = function(targets, annotator_target, max_data_points) {
	//console.info("i am in genarateDataURL");
	var data_targets;
	annotator_target = annotator_target ? "&target=" + machine_name + annotator_target : "";
	//create url part about targets/target	
	data_targets = generateGraphiteTargets(targets);
		return "" + graphite_url + "/render?from=-" + period + "minutes&" + data_targets + annotator_target + "&maxDataPoints=" + 	max_data_points + "&format=json&jsonp=?";
};

//fun-generateEventsURL 
generateEventsURL = function(event_tags) {
	//console.info("i am in generateEventURL");
	var jsonp, tags;
	tags = event_tags === '*' ? '' : "&tags=" + event_tags;
	jsonp = window.json_fallback ? '' : "&jsonp=?";
	return "" + graphite_url + "/events/get_data?from=-" + period + "minutes" + tags + jsonp;
};

//fun-createGraph Create whole graph
createGraph = function(anchor, metric, i) {
	//console.info("i am in createGraph");
	var graph, graph_provider, unstackable, _ref, _ref1, _ref2;  
	//create graph using rickshaw json interface
	console.info($("" + anchor + " #chart").width()-20);
	graph_provider = Rickshaw.Graph.JSONP.Graphite;
	unstackable = (_ref = metric.renderer) === 'line' || _ref === 'scatterplot';
	//console.info("entering graph provider");
	//call graph provider that shall create the graphs with the initialised metrics -svg part only
	return graph = new graph_provider({
		anchor: anchor,
		targets: metric.target || metric.targets,
		summary: metric.summary,
			summary_formatter: metric.summary_formatter || _formatBase1024KMGTP,
		scheme: metric.scheme || dashboard.scheme || scheme || 'classic9',
		annotator_target: ((_ref1 = metric.annotator) != null ? _ref1.target : void 0) || metric.annotator,
		annotator_description: ((_ref2 = metric.annotator) != null ? _ref2.description : void 0) || 'deployment',
			events: metric.events,
		element: $("" + anchor + " #chart")[0],
		width:($("" + anchor + " #chart").width()-'20'),
		height: metric.height || 300,
		min: metric.min || 0,
		max: metric.max,
		null_as: metric.null_as === void 0 ? null : metric.null_as,
		renderer: metric.renderer || 'area',
		interpolation: metric.interpolation || 'step-before',
		unstack: metric.unstack === void 0 ? unstackable : metric.unstack,
		stroke: metric.stroke === false ? false : true,
		strokeWidth: metric.stroke_width,
		//generate the dataURL for the graph
		dataURL: generateDataURL(metric.target || metric.targets),
		onRefresh: function(transport) {
			var slider = new Rickshaw.Graph.RangeSlider( {
				graph: graph,	
				element: $(anchor + '-slider')
			} );
			return refreshSummary(transport);
		},
		onComplete: function(transport, i){
	//		console.info("im in onComplete");
			var detail, hover_formatter, shelving, xAxis, yAxis,slider, highlighter, order;
			graph = transport.graph;		    			
			xAxis = new Rickshaw.Graph.Axis.Time({
				graph: graph		
			});
			xAxis.render();
			yAxis = new Rickshaw.Graph.Axis.Y({
				graph: graph,
				tickFormat: function(y) {
					return _formatBase1024KMGTP(y);
				},
				ticksTreatment: 'glow'
			});
			yAxis.render();  
			hover_formatter = metric.hover_formatter || _formatBase1024KMGTP;
			detail = new Rickshaw.Graph.HoverDetail({
				graph: graph,		
				yFormatter: function(y) {
					return hover_formatter(y);
				}
			});
			$("" + anchor + " .legend").empty();
			this.legend = new Rickshaw.Graph.Legend({
				graph: graph,
				element: $("" + anchor + " .legend")[0]
			});
			shelving = new Rickshaw.Graph.Behavior.Series.Toggle({
				graph: graph,
				legend: this.legend
			});
			highlighter = new Rickshaw.Graph.Behavior.Series.Highlight( {
				graph: graph,
				legend: this.legend
			} );
			order = new Rickshaw.Graph.Behavior.Series.Order( {
				graph: graph,
				legend: this.legend
			} );
			if (metric.annotator || metric.events) {
				this.annotator = new GiraffeAnnotate({
					graph: graph,
					element: $("" + anchor + " .timeline")[0]
				});
			}
			slider = new Rickshaw.Graph.RangeSlider( {
				graph: graph,	
				element: $(anchor + '-slider')
			} );
			return refreshSummary(this);
		}
	});
};


//fun-GraphProvider  actual creation of svg image of graph
try
{
Rickshaw.Graph.JSONP.Graphite = Rickshaw.Class.create(Rickshaw.Graph.JSONP, {
//these are executed to get the graph object for plotting  
	request: function() {
	//	console.info('1');
		return this.refreshGraph(period);
	},
	refreshGraph: function(period) {
	//	console.info('2');
		var deferred,
		_this = this;
		deferred = this.getAjaxData(period);
		//console.info(deferred);
		//this function gets called only after all above objects are obtained
		//then for each object obtained i.e each graph
		return deferred.done(function(result) {
			//console.info(result);
			var annotations, el, i, result_data, series, _i, _len;
			//if empty then return      
			if (result.length <= 0) {
				return;
			}
			//for each target in the list filter the annotator part
			result_data = _.filter(result, function(el) {
			var _ref;
			return el.target !== ((_ref = _this.args.annotator_target) != null ? _ref.replace(/["']/g, '') : void 0);
		});
	//	console.info('5');
		//then preprocess the filtered targets for stack on unstack part
		result_data = _this.preProcess(result_data);
		//then parse the obtained result data to generate svg image      
		if (!_this.graph) {
	//		console.info('6');
			_this.success(_this.parseGraphiteData(result_data, _this.args.null_as));
		}
		series = _this.parseGraphiteData(result_data, _this.args.null_as);
		console.info(series);
		if (_this.args.annotator_target) {
	//		console.info('7');
			annotations = _this.parseGraphiteData(_.filter(result, function(el) {
				return el.target === _this.args.annotator_target.replace(/["']/g, '');
			}), _this.args.null_as);
		}
		//create legend part of the graph and its contents
		for (i = _i = 0, _len = series.length; _i < _len; i = ++_i) {
	//		console.info('8');
			el = series[i];
			_this.graph.series[i].data = el.data;
			_this.addTotals(i);
		}
		_this.graph.renderer.unstack = _this.args.unstack;
		_this.graph.render();
		if (_this.args.events) {
	//		console.info('9');
			deferred = _this.getEvents(period);
			deferred.done(function(result) {
				return _this.addEventAnnotations(result);
			});
		}
		//_this.addAnnotations(annotations, _this.args.annotator_description);
		return _this.args.onRefresh(_this);
	});
},

//list of utility functions used by this main function
addTotals: function(i) {
	//console.info("i am in addTotals");
	var avg, label, max, min, series_data, sum;
	label = $(this.legend.lines[i].element).find('span.label').text();
	$(this.legend.lines[i].element).find('span.totals').remove();
	series_data = _.map(this.legend.lines[i].series.data, function(d) {
		return d.y;
	});
	sum = _formatBase1024KMGTP(_sum(series_data));
	max = _formatBase1024KMGTP(_max(series_data));
	min = _formatBase1024KMGTP(_min(series_data));
	avg = _formatBase1024KMGTP(_avg(series_data));
return $(this.legend.lines[i].element).append("<span class='totals pull-right'> Sum: " + sum + " <i class=#>Min</i>: " + min + " <i class=#>Max</i>: " + max + " <i class=#>Avg</i>: " + avg + "</span>");
},
//fun-preProcess
preProcess: function(result) {
	//console.info("i am in preProcess");
	var item, _i, _len;
	for (_i = 0, _len = result.length; _i < _len; _i++) {
		item = result[_i];
		if (item.datapoints.length === 1) {
			item.datapoints[0][1] = 0;
			if (this.args.unstack) {
				item.datapoints.push([0, 1]);
			} else {
				item.datapoints.push([item.datapoints[0][0], 1]);
			}
		}
	}
	return result;
},

parseGraphiteData: function(d, null_as) {
	//console.info("i am in parseGraphite Data");
	var palette, rev_xy, targets;
	if (null_as == null) {
		null_as = null;
	}
	//map datapoints for x and y part of the series requirement for each object of rickshaw
	rev_xy = function(datapoints) {
		return _.map(datapoints, function(point) {
			return {
				'x': point[1],
				'y': point[0] !== null ? point[0] : null_as
			};
		});
	};
	//create a palette for color for each target   
	palette = new Rickshaw.Color.Palette({
		//scheme: this.args.scheme
	});
	targets = this.args.target || this.args.targets;
	//create a proper object for each target in series assigning proper color
	d = _.map(d, function(el) {
		var color, _ref;
		if ((_ref = typeof targets) === "string" || _ref === "function") {
			color = palette.color();
		} else {
		//get target color for each target in series
			color = getTargetColor(targets, el.target) || palette.color();
		}
		return {
			"color": color,
			"name": el.target,
			"data": rev_xy(el.datapoints)
		};
	});
	//create svg image for graph
	Rickshaw.Series.zeroFill(d);
	return d;
},
//fun-addEventAnnotations
addEventAnnotations: function(events_json) {
	//console.info("i am in addEventAnnotations");
	var active_annotation, event, _i, _len, _ref, _ref1;
	if (!events_json) {
		return;
	}
	this.annotator || (this.annotator = new GiraffeAnnotate({
	graph: this.graph,
	element: $("" + this.args.anchor + " .timeline")[0]
	}));
	this.annotator.data = {};
	$(this.annotator.elements.timeline).empty();
	active_annotation = $(this.annotator.elements.timeline).parent().find('.annotation_line.active').size() > 0;
	if ((_ref = $(this.annotator.elements.timeline).parent()) != null) {
		_ref.find('.annotation_line').remove();
	}
	for (_i = 0, _len = events_json.length; _i < _len; _i++) {
		event = events_json[_i];
		this.annotator.add(event.when, "" + event.what + " " + (event.data || ''));
	}
	this.annotator.update();
	if (active_annotation) {
		return (_ref1 = $(this.annotator.elements.timeline).parent()) != null ? _ref1.find('.annotation_line').addClass	('active') : void 0;
	}
},
//fun-addAnnotations
addAnnotations: function(annotations, description) {
	//console.info("i am in addAnnotations");
	var annotation_timestamps, _ref;
	if (!annotations) {
		return;
	}
	annotation_timestamps = _((_ref = annotations[0]) != null ? _ref.data : void 0).filter(function(el) {
		return el.y !== 0 && el.y !== null;
	});
	return this.addEventAnnotations(_.map(annotation_timestamps, function(a) {
		return {	
			when: a.x,
			what: description
		};
	}));
},
//fun-getEvents
getEvents: function(period) {
	//console.info("i am in getEvents");
	var deferred,
	_this = this;
	this.period = period;
	return deferred = $.ajax({
		dataType: 'json',
		url: generateEventsURL(this.args.events),
		error: function(xhr, textStatus, errorThrown) {
			if (textStatus === 'parsererror' && /was not called/.test(errorThrown.message)) {
				window.json_fallback = true;
				return _this.refreshGraph(period);
			} else {
				return console.log("error loading eventsURL: " + generateEventsURL(_this.args.events));
			}
		}
	});
},
//fun-getAjaxData
getAjaxData: function(period) {	
	//console.info("i am in getajaxData");
	var deferred;
	this.period = period;
	return deferred = $.ajax({
		dataType: 'json',
		url: generateDataURL(this.args.targets, this.args.annotator_target, this.args.width),
		error: this.error.bind(this)
	});
}
});
}catch(e){
	console.error(e);
}


//On click changes -events and interactions
/*$('.dropdown-menu').on('click', 'a', function() {
	changeDashboard($(this).text());
	$('.dropdown').removeClass('open');
	return false;
});*/

changeDashboard = function(dash_name) {
	dashboard = _.where(dashboards, {
		name: dash_name
	})[0] || dashboards[0];
	graphite_url = dashboard['graphite_url'] || default_graphite_url;
	description = dashboard['description'];
	metrics = dashboard['metrics'];
	refresh = dashboard['refresh'];
	period || (period = default_period);
	init();
	return $.bbq.pushState({
		dashboard: dashboard.name
	});
};

$('.timepanel').on('click', 'a.range', function() {
	var dash, timeFrame, _ref;
	if (graphite_url === 'demo') {
		changeDashboard(dashboard.name);
	}
	period = $(this).attr('data-timeframe') || default_period;
	dataPoll();
	timeFrame = $(this).attr('href').replace(/^#/, '');
	dash = (_ref = $.bbq.getState()) != null ? _ref.dashboard : void 0;
	$.bbq.pushState({
		timeFrame: timeFrame,
		dashboard: dash || dashboard.name
	});
	$(this).parent('.btn-group').find('a').removeClass('active');
	$(this).addClass('active');
	return false;
});

toggleCss = function(css_selector) {
	if ($.rule(css_selector).text().match('display: ?none')) {
		return $.rule(css_selector, 'style').remove();
	} else {
		return $.rule("" + css_selector + " {display:none;}").appendTo('style');
	}
};

$('#legend-toggle').on('click', function() {
	$(this).toggleClass('active');
	$('.legend').toggle();
	return false;
});

$('#axis-toggle').on('click', function() {
	$(this).toggleClass('active');
	toggleCss('.y_grid');
	toggleCss('.y_ticks');
	toggleCss('.x_tick');
	return false;
});

$('#x-label-toggle').on('click', function() {
	toggleCss('.rickshaw_graph .detail .x_label');
	$(this).toggleClass('active');
	return false;
});

$('#x-item-toggle').on('click', function() {
	toggleCss('.rickshaw_graph .detail .item.active');
	$(this).toggleClass('active');
	return false;
});

$(window).bind('hashchange', function(e) {
	console.info(e);
	var pattern = new RegExp("graph");
	if(pattern.test(e.fragment))
		return;
	var dash, timeFrame, _ref, _ref1;
	timeFrame = ((_ref = e.getState()) != null ? _ref.timeFrame : void 0) || $(".timepanel a.range[data-timeframe='" + default_period + "']")[0].text || "1d";
	dash = (_ref1 = e.getState()) != null ? _ref1.dashboard : void 0;
	if (dash !== dashboard.name) {
		changeDashboard(dash);
	}
	return $('.timepanel a.range[href="#' + timeFrame + '"]').click();
});



$(function() {
	console.info("I am trigerring hash");
	$(window).trigger('hashchange');
	return init();
});
