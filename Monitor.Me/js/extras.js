var my;var tags=[];var extra=[];var saved=[];var temp=[];
var times=["10m","1h","3h","12h","1d","1w","2w"];

/*----------------------------------------initialisation------------------------------------------*/
//initialises parameters for configuration change of graph
var InitParams=function(Id){
	$("#graphN").val(dashboards[0].metrics[Id].alias);
	$("#graphR").val(dashboards[0].metrics[Id].renderer);
	$("#graphI").val(dashboards[0].metrics[Id].interpolation);			
	$("#graphD").val(dashboards[0].metrics[Id].description);
	var size=$("#graphS").val();
	if(size === 1)
		size="Small";
	else if(size === 1.5)
		size="Half";
	else if(size === 2)
		size="Large";
	else if(size === 3)
		size="Full";
	$("#graphS").val(size);
	$("#graphId").val(Id);
	$('#deleteList').empty();
	if(metrics[Id].targets.length === 0){
		$('#deleteList').append("<div class=\"alert alert-success\">Currently there are metrics on your graphs.</div>");
	}
	else {
	for (var i = 0;i < metrics[Id].targets.length; i++) {
		$('#deleteList').append("<li data-id=\""+i+"\" data-g-id="+Id+" rel='tooltip' data-placement='bottom' onClick=\"deleteMetrics(this);\" title=\"Click to delete this metric from graph \"style=\"margin-right:3px;\"><a style=\"border:1px solid burlywood;background: #2e3236;\">"+metrics[Id].targets[i]+"</a></li>");
	}
	$("[rel='tooltip']").tooltip();
	}

};

//initialises list of graphs in main configuration modal
var initGraphList=function(){
	$('#graphList').empty();
	if(metrics.length === 0){
		$('#graphList').append("<div class=\"alert alert-success\">Currently there are no graphs on your dashboard.</div>");
	}
	else {
	for (var i = 0;i < metrics.length; i++) {
		$('#graphList').append("<li data-id=\""+i+"\" rel='tooltip' data-placement='bottom'  onClick=\"removeGraph1(this);\" title=\"Click to delete this graph from dashboard\"style=\"margin-right:3px;\"><a style=\"border:1px solid burlywood;background: #2e3236;\">"+metrics[i].alias+"<i class=\"icon-trash icon-large pull-right\" style=\"margin-top:2px;margin-left:20px;\"></i></a></li>");
	}
	$("[rel='tooltip']").tooltip();
	}
	
};

//initialisation of modal input values on their respective clicks
var initModal=function(id){
	if(id===1){//Config
		$("#configDashboardName").val(dashboards[0].name);
		$("#configGraphName1").val("");
		$("#configGraphDesc1").val("");
		temp = times.slice(0);
		temp.toString();
		$("#timeResolutions").val(temp);
		$("#refreshRate").val(dashboards[0].refresh);
	}
	else if(id === 2){//Save
		$("#dashName").val("");
	}
	else if(id === 3){//Load
		loadNames();
		var id=document.getElementById("Myid");
		id.selectedIndex = 0;
	}
	else if(id === 4){//Addgraph
		
		$("#graphName").val("");
		$("#graphDesc").val("");
	}
};

//function to initialise the load dashboards names list
var completeSaved=function(){
$('#Myid').empty();
	//find dashboards and init dropdown
	for (i = 0;i< saved.length;i++) {
		$('#Myid').append("<option>" + saved[i] + "</option>");
	}
	var id=document.getElementById("Myid");
	id.selectedIndex = 0;
};

/*---------------------------------------graph manipulation---------------------------------------*/
//function to add a metric to the selected graph
var addToGraph=function(id){
	var metricChoice=(document.getElementById("input-"+id)).value;
	if(dashboards[0].metrics[id].source.indexOf("Graphite")>-1){
		if(metricChoice&&validMetric(metricChoice)){
			if(!ifExists(metricChoice,id))
			{
				dashboards[0].metrics[id].targets.push(metricChoice);
				pseudoInit();
			}
			else{
				alert("A metric with the same name already exists in the graph.You cannot add two metrics with same names.");
				$("#input-"+id).val("");
				$("#input-"+id).focus();
			}
		}
		else{
			alert("Error in entering the metric name.Not a valid metric name.");
			$("#input-"+id).val("");
			$("#input-"+id).focus();
			
		}
	}
	else{
		dashboards[0].metrics[id].targets.push(metricChoice);
		pseudoInit();
	}
};
//function to remove the graph from the dashboard
var removeGraph=function(id){
	if (confirm("Are you sure you want to delete "+dashboards[0].metrics[id].alias+" from the dashboard?")) {
		dashboards[0].metrics.splice(id,1);
		pseudoInit();
	}
};

//function to remove graph from the configure modal
var removeGraph1=function(obj){
	var id=obj.getAttribute("data-id");
	if (confirm("Are you sure you want to delete "+dashboards[0].metrics[id].alias+" from the dashboard?")) {
		dashboards[0].metrics.splice(id,1);
		pseudoInit();
	}
};

//function to delete metrics from coressponding graph
var deleteMetrics=function(obj){
	var id=obj.getAttribute("data-id");
	var gid=obj.getAttribute("data-g-id");
	if (confirm("Are you sure you want to delete "+dashboards[0].metrics[gid].targets[id]+" from the graph?")) {
		dashboards[0].metrics[gid].targets.splice(id,1);
		pseudoInit();
		InitParams(gid);
	}
};

var getSize=function(size){
	if(size.indexOf("Small")>-1){
		return 1;
	}
	else if(size.indexOf("Half")>-1){
		return 1.5;
	}
	else if(size.indexOf("Large")>-1){
		return 2;
	}
	else if(size.indexOf("Full")>-1){
		return 3;
	}
}

//function to add a graph separately from icon on dashboard 
$( "#addModal" ).submit(function( event ) {
	if(!ifTargetsEmpty()){
		if($("#graphName").val())
		{
			var obj={
				alias:$("#graphName").val(),
      		    targets: [],   
 				events: "*",
        		renderer:$("#graphRenderer").val(), 
				interpolation :$("#graphInterpol").val(),
				description:$("#graphDesc").val(),
				hover_formatter: d3.format("03.6g"),
				summary: "sum",
				totals_fields: ["min", "max"],  
				height:240,
				width:465,
				colspan:getSize($("#graphSize").val()) || 1.5,
				source:$("#graphSource").val(),
				unstack: false
			};
		dashboards[0].metrics.push(obj);
		pseudoInit();
	}
	else{
		alert("Please enter a title for the graph.");
	}
}
});	

//function to add a graph from the main configuration modal
$( "#addModal1" ).submit(function( event ) {
	if(!ifTargetsEmpty()){
		if($("#configGraphName1").val())
		{
			var obj={
				alias:$("#configGraphName1").val(),
        		targets: [],   
        		events: "*",
        		renderer:$("#configGraphRenderer1").val(), 
				interpolation :$("#configGraphInterpol1").val(),
				description:$("#configGraphDesc1").val(),
				hover_formatter: d3.format("03.6g"),
				summary: "sum",
				height:240,
				width:465,
				source:$("#configGraphSource").val(),
				colspan:getSize($("#configGraphSize").val()) || 1.5,
				unstack: false
			};
			dashboards[0].metrics.push(obj);
			pseudoInit();
		}
		else{
			alert("Please enter a title for the graph.");
		}
	}

});	

//function called on click to save dashboard
$( "#Save" ).submit(function( event ) {	
	var name;
	name=$("#dashName").val();
	findExists(name);
	if(name){ //if name is not empty
		if(metrics.length){//contains at least one graph
			if(!findExists(name)){//if same name does not exist
				saveDashboard("../Saved/"+name+".json");//call to actual save
			}
			else{
				alert("A dashboard with the same name exists.Please choose another name.");
			}
		}
		else {
			alert("There are no graphs added to dashboard.You are saving the default config again!!");
		}
	}
	else{
		alert("Please provide a valid name for the dashboard");
	}
});	

//function to load a specific dashboard 
$( "#Load" ).submit(function(event) {	
	var name=$("#Myid").val();
	if(name){//if dashboard is chosen
	$.getJSON("../Saved/"+name, function(data) {
		dashboards=[];
		dashboards=data;
		pseudoInit();	
	});
	}
	else{
		alert("Please choose a dashboard to load.");
	}
});	

//function called on click in the graphs configuration change
$( "#changeGraph" ).submit(function(event) {	
	var graphid=$("#graphId").val();
	console.log($("#graphS").val());
	dashboards[0].metrics[graphid].alias=$("#graphN").val();
	dashboards[0].metrics[graphid].renderer=$("#graphR").val();
	dashboards[0].metrics[graphid].interpolation=$("#graphI").val();
	dashboards[0].metrics[graphid].description=$("#graphD").val();
	dashboards[0].metrics[graphid].colspan=getSize($("#graphS").val());
	pseudoInit();
});	

//function called on click to dashboard name update
$( "#Dname" ).submit(function(event) {	
	dashboards[0].name=$("#configDashboardName").val();
	dashboard=dashboards[0];
	metrics = dashboard['metrics'];
	$('.page-header h1').empty().append((dashboard.name).toUpperCase());
});	

//function to change the timepicker according to the user
$( "#TimePicker" ).submit(function(event) {	
	var temp="",index=0;
	var arr1=[];
	if($("#timeResolutions").val()){
		var arr=$("#timeResolutions").val();
		var arr1 = arr.split(',');
		$('.dropdown-menu').empty();
		for (var i = 0;i < arr1.length; i++) {
		$('.dropdown-menu').append("<li data-timeframe=\""+ arr1[i] +"\" onClick=\"changeTime(this)\"><a>" + arr1[i] + "</a></li>");
		}
		times=arr1;
	}
	else{
		alert("Please enter some value for time resolutions.");
	}
	if($("#refreshRate").val()){
		dashboards[0].refresh=$("#refreshRate").val();
	}
	else{
		alert("Please enter some value for refresh rate.");
	}
});	

/*------------------------------------------------metrics autocomplete--------------------------------------------*/
//main autocomplete function
var autoComplete=function(num){
	$(function() {
    	for(i=0;i<num;i++){
    		var model=$("#input-"+i).data("model");
    		if(model.indexOf("Kpoint")>-1){
				$("#input-"+i).autocomplete({
    				source: kpointMetrics
    			});
			}
			else{
				$("#input-"+i).autocomplete({
    				source: tags
    			});
   			}
   		}
    	//$("#graphMetrics").autocomplete({
		//	source:tags
    	//});	
  });
};

//function to get a list of metrics from graphite
var getMainList = function(URL){
	$.getJSON( URL, function(data) {
		for(i=0;i<data.length;i++){
		tags[i]=data[i];}
		my=i;
	});
	
};

//custom function to get a list of metrics from munin if configured
var getMunin=function(){
	$.getJSON( graphite_url+"/metrics/find?query="+machine_name_munin+".*", function(data) {
		var j=0;
		for(i=0;i<data.length;i++){
		extra[i]=data[i].id+".42";}
		for(i=my;i<my+data.length;i++){		
			tags[i]=extra[j++];
		}
	});
};

//utility function to call autcomplete functions
var initAutocomplete = function(){
	var URL;
	URL=""+ graphite_url + "/metrics/index.json";
	getMainList(URL);
	if(machine_name_munin){
		getMunin();
	}
};

/*----------------------------------------backend-------------------------------------------*/
//function to actually save dashboard to decided folder
var saveDashboard=function(dname){
	$.ajax({
	url:"my1.php",
	type:"post",
	data:{value:JSON.stringify(dashboards),name:dname,id:"save"},   //Converts a JavaScript value to a JavaScript Object Notation (JSON) string.
	success:function(result){
		alert("Dashboard has been saved by the name"+dname);
	}
});
	loadNames();
};
var i=0;
//function to load names of the dashboards that are already present or saved by the user
var loadNames=function(){
	$.ajax({
	url:"my1.php",
	type:"post",
	data:{value:JSON.stringify(dashboards),name:"",id:"load"},   //Converts a JavaScript value to a JavaScript Object Notation (JSON) string.
	success:function(data){
		//alert("Dashboard has been loaded..oh yes");
		i=0;
		var obj=JSON.parse(data);
		for (var prop in obj){
			saved[i++]=obj[prop];
		}
		completeSaved();
	}
});

};

//utility function to check if the same name dashboard already exists
var findExists=function(name){
	for(var i=0;i<saved.length;i++){
		if(saved[i].indexOf(name)>-1){
			return true;
		}
	}
	return false;
};

//utility function to toggle the query bar hide/show
var Togg=function(id){
	var row=".row-"+id;
	$(row).toggle(500);
};

//function to plot same graph at different time resolutions on click on dropdown
var changeTime=function(obj){
	var timeframe=obj.getAttribute("data-timeframe");
	var time=timeframe.substring(0,timeframe.length-1);
	var system=timeframe.substring(timeframe.length-1,timeframe.length);
	var mul;
	if(system.valueOf() === "m"){
		mul=time;
	}
	else if(system.valueOf() === "h"){
		mul=time*60;
	}
	else if(system.valueOf() === "d"){
		mul=time*60*24;
	}
	else if(system.valueOf() === "w"){
		mul=time*60*24*7;
	}
	period = mul;
	dashboard = dashboards[0];
	metrics = dashboard['metrics'];
	init();
};

var ifTargetsEmpty=function(){
	for(var i=0;i<metrics.length;i++){
		if(metrics[i].targets.length === 0){
			alert("Please add metrics to one of the graphs.It is empty.");
			return 1;
		}
	}
	return 0;
};

var ifExists=function(name,id){
	for(var i=0;i<metrics[id].targets.length;i++){
		if(metrics[id].targets[i].indexOf(name)>-1){
			return true;
		}
	}
	return false;
};

var validMetric=function(name){
	for(var i=0;i<tags.length;i++){
		if(tags[i] === name){
			return true;
		}
	}
	return false;
};

var createGraphiteLike=function(names,targ){
	var main=[];var arr=[];var values;
	for (var prop in names){
		var obj=
		{
			datapoints:[],
			target:"",
		};
		values=names[prop];
		obj.target=prop;
		for(var prop1 in values){
			arr[1]=Number(prop1);
			arr[0]=parseInt(values[prop1]);
			obj.datapoints.push([arr[0],arr[1]]);
		}	
		main.push(obj);
	}
	return main;
};

var kpointMetricList=function(){
	$.ajax({
	url:"db.php",
	type:"post",
	data:{type:"names",target:"",user:kpoint_db.username,pass:kpoint_db.password,host:kpoint_db.hostname,db:kpoint_db.database},
	success:function(data){
		kpointMetrics=JSON.parse(data);
	}
});
};

var kpointAjaxData=function(targ){
	return deferred=$.ajax({
	url:"db.php",
	type:"post",
	data:{type:"data",target:targ,user:kpoint_db.username,pass:kpoint_db.password,host:kpoint_db.hostname,db:kpoint_db.database},
	});	
};

var pseudoInit = function(){
	period = default_period;
	dashboard = dashboards[0];
	metrics = dashboard['metrics'];
	refresh = dashboard['refresh'];
	init();
};
/*
1.Add metrics kartana  graph madhe baghaycha ki already to metrics added nahiye na.==done for graphite not for kpoint
2.get munin function ha call jhala pahije if and only if config == done
3.period for kpointdb
4.validations on timescales and user entered values
5.check if config params are given
6.error handling on requests and other stuff
7.add other user based params-refresh,totals,formatters,stack etc....
*/