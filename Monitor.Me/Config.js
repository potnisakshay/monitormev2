//pre-requisites
//cross-origin requests must be enabled on the server or your browser

var graphite_url = "http://192.168.1.100";//enter your graphite url here

//if you want to see munin graphs that are soft linked to /opt/graphite/storage/rrd 
//set the variable below to the directory structure to your munin metrics
//eg:monitorme.monitorme-apache_accesses-accesses80-d.42 the
//machine_name_munin variable shall be set to machine_name_munin="monitorme";
var machine_name_munin="monitorme";

//mandatory variables for kpoint db
var kpoint_db = "";
 /*{
	"username":"root",
	"password":"root",
	"hostname":"localhost",
	"database":"myTest",
};*/

