var auth, changeDashboard, createGraph, dashboard, dataPoll, default_graphite_url, default_period, description, generateDataURL, generateEventsURL, generateGraphiteTargets, getTargetColor, graphScaffold, graphite_url, graphs, init, metrics, period, refresh, refreshSummary, refreshTimer, scheme, toggleCss, _avg, _formatBase1024KMGTP, _last, _max, _min, _sum, machine_name;
var num;var kpointMetrics=[];var kpoint=[];
var dashboards = 
[
  { "name": "System", 
    "refresh": 500000,//this is in milliseconds   
    "metrics":  
    []
  },
];
default_graphite_url = graphite_url;
default_period = 1440;
if (scheme === void 0) {
	scheme = 'colorwheel';
}
period = default_period;
dashboard = dashboards[0];
metrics = dashboard['metrics'];
description = dashboard['description'];
refresh = dashboard['refresh'];
refreshTimer = null;
auth = auth != null ? auth : false;
graphs = [];


__indexOf = [].indexOf || function(item) { 
	for (var i = 0, l = this.length; i < l; i++) { 
		if (i in this && this[i] === item) return i;
	} 
	return -1; 
};

dataPoll = function() {
	var graph, _i, _len, _results;
	_results = [];
	for (_i = 0, _len = graphs.length; _i < _len; _i++) {
		graph = graphs[_i];
		_results.push(graph.refreshGraph(period));
	}
	return _results;
};

_sum = function(series) {
	return _.reduce(series, (function(memo, val) {
		return memo + val;
	}), 0);
};

_avg = function(series) {
	return _sum(series) / series.length;
};

_max = function(series) {
	return _.reduce(series, (function(memo, val) {
		if (memo === null) {
			return val;
		}
		if (val > memo) {
			return val;
		}
		return memo;
	}), null);
};

_min = function(series) {
	return _.reduce(series, (function(memo, val) {
		if (memo === null) {
			return val;
		}
		if (val < memo) {
			return val;
		}
		return memo;
	}), null);
};

_last = function(series) {
	return _.reduce(series, (function(memo, val) {
		if (val !== null) {
			return val;
		}
		return memo;
	}), null);
};

_formatBase1024KMGTP = function(y, formatter) {
	var abs_y;
	if (formatter == null) {
		formatter = d3.format(".2r");
	}
	abs_y = Math.abs(y);
	if (abs_y >= 1125899906842624) {
		return formatter(y / 1125899906842624) + "P";
	} else if (abs_y >= 1099511627776) {
		return formatter(y / 1099511627776) + "T";
	} else if (abs_y >= 1073741824) {
		return formatter(y / 1073741824) + "G";
	} else if (abs_y >= 1048576) {
		return formatter(y / 1048576) + "M";
	} else if (abs_y >= 1024) {
		return formatter(y / 1024) + "K";
	} else if (abs_y < 1 && y > 0) {
		return formatter(y);
	} else if (abs_y === 0) {
		return 0;
	} else {
		return formatter(y);
	}
};

refreshSummary = function(graph) {
	var summary_func, y_data, _ref;
	if (!((_ref = graph.args) != null ? _ref.summary : void 0)) {
		return;
	}
	if (graph.args.summary === "sum") {
		summary_func = _sum;
	}
	if (graph.args.summary === "avg") {
		summary_func = _avg;
	}
	if (graph.args.summary === "min") {
		summary_func = _min;
	}
	if (graph.args.summary === "max") {
		summary_func = _max;
	}
	if (graph.args.summary === "last") {
		summary_func = _last;
	}
	if (typeof graph.args.summary === "function") {
		summary_func = graph.args.summary;
	}
	if (!summary_func) {
		console.log("unknown summary function " + graph.args.summary);
	}
	y_data = _.map(_.flatten(_.pluck(graph.graph.series, 'data')), function(d) {
		return d.y;
	});
	return $("" + graph.args.anchor + " .graph-summary").html(graph.args.summary_formatter(summary_func(y_data)));
};

graphScaffold = function() 
{
	var colspan, context, graph_template, i, metric, offset, _i, _len;
	graph_template = "{{#dashboard_description}}\n"
			+"<div class=\"well\">{{{dashboard_description}}}</div>\n"
			+"{{/dashboard_description}}\n{{#metrics}}\n "
			+"{{#start_row}}\n"
			+"<div class=\"row-fluid\" style=\"margin-top:30px;\">\n  {{/start_row}}\n "
			+"<div class=\"{{span}}\" id=\"graph-{{graph_id}}\" style=\"padding-left:23px;outline: 1px solid #101214;border-top: 1px solid #3e444c;background:#202328;margin-top:20px;\" >\n "
			+"<h4 style=\"margin-top:0px;margin-bottom:15px;font-size:11px;margin-left:-16px;color:#c8c8c8;\">{{metric_alias}}"
			+"<span class=\"pull-right \" style=\"margin-right:4px;margin-top:2px;\">"
			+"<a class=\"icon-search icon-large\" style=\"font-size:12px;color:#c8c8c8;border-left: 1px solid #666;border-right: 1px solid #101214;border-bottom: 1px solid #666;padding: 5px 7px 2px;margin-right:-5px;\" rel=\'tooltip\' data-placement=\'top\' title=\"Show/Hide Query bar\" onClick=\"Togg({{graph_id}})\"></a>&nbsp&nbsp"
			+"<a class=\"icon-cog icon-large\" style=\"font-size:12px;color:#c8c8c8;border-left: 1px solid #666;border-right: 1px solid #101214;border-bottom: 1px solid #666;padding: 5px 7px 2px;margin-right:-5px;\" rel=\'tooltip\' data-placement=\'top\' data-reveal-id=\"Graph\" title=\"Configure Graph\" onClick=\"InitParams({{graph_id}})\"></a>&nbsp&nbsp"			
			+"<a class=\"icon-remove-sign icon-large\" style=\"font-size:12px;color:#c8c8c8;border-left: 1px solid #666;border-right: 1px solid #101214;border-bottom: 1px solid #666;padding: 5px 7px 2px;margin-right:-5px;\" rel=\'tooltip\' data-placement=\'top\' title=\"Remove graph\" id=\"close-{{graph_id}}\" data-id=\"{{graph_id}}\" onClick=\"removeGraph({{graph_id}})\" \"></a>"	
			+"</span></h4>\n"
			+"<div class=\"row-{{graph_id}}\" style=\"margin-left:-2px;\" id=\"inp\">"
			+"<input id=\"input-{{graph_id}}\"type=\"text\" class=\"form-control\" data-model=\"Graphite\"/ placeholder=\"Add metrics to Graph\" rel='tooltip' data-placement='top' title=\"Autocomplete metric querying filter.\" style=\"padding-left:10px;border-radius:20px;width:86%;background-color: #52575c;;color:aliceblue;\"/>"
			+"<a class=\"icon-plus\" style=\"font-size:18px;color:#c8c8c8;margin-left:5px;\" rel='tooltip' data-placement='bottom' title=\"Click to add metric to Graph\" onCLick=\"addToGraph({{graph_id}})\"></a>&nbsp&nbsp"		
			+"</div>"
			+"<div id=\"graph-{{graph_id}}-smoother\" style=\"width:35% !important;background:inherit !important\"></div>"
			+"<div id=\"chart\" style=\"margin-top:20px;\"></div>\n"
			+"<div class=\"timeline\"></div>\n\n\n "
			+"<div id=\"graph-{{graph_id}}-slider\" style=\"margin-top:17px;background:inherit !important\" ></div> "
			+"<h4 style=\"font-weight:bold;font-size:11px;margin-top:-15px;color:#c8c8c8;margin-left:-7px;\"id=\"me-{{graph_id}}\">{{metric_description}}"
			+"<span class=\"pull-right graph-summary\" style=\"margin-right:10px;margin-top:2px;\">"
			+"</h4>\n\n"
			+"<div class=\"legend\"></div>\n\n\n"
			+"</div>\n  {{#end_row}}\n  </div>\n  {{/end_row}}\n{{/metrics}}";
	$('#graphs').empty();
	context = {
		metrics: []
	};	
	offset = 0;
	for (i = _i = 0, _len = metrics.length; _i < _len; i = ++_i) {
		metric = metrics[i];
		colspan = metric.colspan != null ? metric.colspan : 1;
		if(metric.description === ""){
			metric.description="This is default description";
		}
		context['metrics'].push({
			start_row: offset % 3 === 0,
			end_row: offset % 3 === 2,
			graph_id: i,
			span: 'span' + (4 * colspan),
			metric_alias: metric.alias.toUpperCase(),
			metric_description: metric.description.toUpperCase()
		});
		offset += colspan;
	}
	return $('#graphs').append(Mustache.render(graph_template, context));
};


init = function() {
	var dash, i, metric, refreshInterval, _i, _j, _len, _len1;
	initGraphList();
	if(kpoint_db){
		//kpointMetricList();
	}
	else{
		$('#graphSource').empty();
		$('#graphSource').append("<option>Graphite</option>");
		$("#configGraphSource").empty();
		$('#configGraphSource').append("<option>Graphite</option>");
	}
	$('.dropdown-menu').empty();
	for (_i = 0;_i < times.length; _i++) {
		$('.dropdown-menu').append("<li data-timeframe=\""+ times[_i] +"\" onClick=\"changeTime(this)\"><a>" + times[_i] + "</a></li>");
	}
	initAutocomplete();
	graphScaffold();
	graphs = [];
	for (i = _j = 0, _len1 = metrics.length; _j < _len1; i = ++_j) {
		metric = metrics[i];
		if(metric.source.indexOf("Kpoint")>-1){
				$("#input-"+i).data("model","Kpoint");
		}
		graphs.push(createGraph("#graph-" + i, metric,i));
	}	
	$('.page-header h1').empty().append((dashboard.name).toUpperCase());
	refreshInterval = refresh || 10000;
	autoComplete(i);
	if(metrics.length === 0){
		$('#graphs').append("<h4 style=\"font-weight:initial;margin-top:60px;margin-left:-36px;\" align=\"center\"> There are no graphs configured on this dashboard. Click on Add graphs to create your custom dashboard.</h4>");
	}
	if (refreshTimer) {
		clearInterval(refreshTimer);
	}
	$("[rel='tooltip']").tooltip();
	return refreshTimer = setInterval(dataPoll, refreshInterval);
};

getTargetColor = function(targets, target) {
	var t, _i, _len;
	if (typeof targets !== 'object') {
		return;
	}
	for (_i = 0, _len = targets.length; _i < _len; _i++) {
		t = targets[_i];
		if (!t.color) {
			continue;
		}
		if (machine_name + t.target === target || t.alias === target) {
			return t.color;
		}
	}
};

generateGraphiteTargets = function(targets) {
	var graphite_targets, target, _i, _len;
	if (typeof targets === "string") {
		return "&target=" + targets;
	}
	if (typeof targets === "function") {
		return "&target=" + (targets());
	}
	graphite_targets = "";
	for (_i = 0, _len = targets.length; _i < _len; _i++) {
		target = targets[_i];
		if (typeof target === "string") {
			graphite_targets += "&target=" + target;
		}
		if (typeof target === "function") {
			graphite_targets += "&target=" + (target());
		}
		if (typeof target === "object") {
			graphite_targets += "&target=" + ((target != null ? target.target : void 0) || '');
		}
	}
	return graphite_targets;
};

generateDataURL = function(targets, annotator_target, max_data_points) {
	var data_targets;
	annotator_target = annotator_target ? "&target=" + machine_name + annotator_target : "";
	data_targets = generateGraphiteTargets(targets);
		return "" + graphite_url + "/render?from=-" + period + "minutes&" + data_targets + annotator_target + "&maxDataPoints=" + 	max_data_points + "&format=json&jsonp=?";
};

generateEventsURL = function(event_tags) {
	var jsonp, tags;
	tags = event_tags === '*' ? '' : "&tags=" + event_tags;
	jsonp = window.json_fallback ? '' : "&jsonp=?";
	return "" + graphite_url + "/events/get_data?from=-" + period + "minutes" + tags + jsonp;
};


createGraph = function(anchor, metric, i) {
	var graph, graph_provider, unstackable, _ref, _ref1, _ref2;  
	graph_provider = Rickshaw.Graph.JSONP.Graphite;
	unstackable = (_ref = metric.renderer) === 'line' || _ref === 'scatterplot';
	return graph = new graph_provider({
		anchor: anchor,
		source:metric.source,
		targets: metric.target || metric.targets,
		summary: metric.summary,
		summary_formatter: metric.summary_formatter || _formatBase1024KMGTP,
		totals_formatter: metric.totals_formatter || _formatBase1024KMGTP,
		totals_fields: metric.totals_fields || ["min", "max", "avg"],
		scheme: metric.scheme || dashboard.scheme || scheme || 'colorwheel',
		annotator_target: ((_ref1 = metric.annotator) != null ? _ref1.target : void 0) || metric.annotator,
		annotator_description: ((_ref2 = metric.annotator) != null ? _ref2.description : void 0) || 'deployment',
		events: metric.events,
		element: $("" + anchor + " #chart")[0],
		width:($("" + anchor + " #chart").width()-'20'),
		height: metric.height || 300,
		min: metric.min || 0,
		max: metric.max,
		null_as: metric.null_as === void 0 ? null : metric.null_as,
		renderer: metric.renderer || 'area',
		interpolation: metric.interpolation || 'step-before',
		unstack: metric.unstack === void 0 ? unstackable : metric.unstack,
		stroke: metric.stroke === false ? false : true,
		strokeWidth: metric.stroke_width,
		dataURL: generateDataURL(metric.target || metric.targets),
		onRefresh: function(transport) {
			var slider = new Rickshaw.Graph.RangeSlider( {
				graph: graph,	
				element: $(anchor + '-slider')
			} );
			return refreshSummary(transport);
		},
		onComplete: function(transport, i){
			console.info("im in onComplete");
			var detail, hover_formatter, shelving, xAxis, yAxis,slider, highlighter, order;
			graph = transport.graph;		    			
			xAxis = new Rickshaw.Graph.Axis.Time({
				graph: graph		
			});
			xAxis.render();
			yAxis = new Rickshaw.Graph.Axis.Y({
				graph: graph,
				tickFormat: function(y) {
					return _formatBase1024KMGTP(y);
				},
				ticksTreatment: 'glow'
			});
			yAxis.render();  
			hover_formatter = metric.hover_formatter || _formatBase1024KMGTP;
			detail = new Rickshaw.Graph.HoverDetail({
				graph: graph,		
				yFormatter: function(y) {
					return hover_formatter(y);
				}
			});
			$("" + anchor + " .legend").empty();
			this.legend = new Rickshaw.Graph.Legend({
				graph: graph,
				element: $("" + anchor + " .legend")[0]
			});
			shelving = new Rickshaw.Graph.Behavior.Series.Toggle({
				graph: graph,
				legend: this.legend
			});
			highlighter = new Rickshaw.Graph.Behavior.Series.Highlight( {
				graph: graph,
				legend: this.legend
			} );
			order = new Rickshaw.Graph.Behavior.Series.Order( {
				graph: graph,
				legend: this.legend
			} );
			if (metric.annotator || metric.events) {
				this.annotator = new GiraffeAnnotate({
					graph: graph,
					element: $("" + anchor + " .timeline")[0]
				});
			}
			$(anchor + '-smoother').empty();
			var smoother = new Rickshaw.Graph.Smoother( {
				graph: graph,
				element: $(anchor + '-smoother')
			} );

			$(anchor + '-slider').empty();
			slider = new Rickshaw.Graph.RangeSlider( {
				graph: graph,	
				element: $(anchor + '-slider')
			} );
			return refreshSummary(this);
		}
	});
};


try
{
Rickshaw.Graph.JSONP.Graphite = Rickshaw.Class.create(Rickshaw.Graph.JSONP, {
	request: function() {
		return this.refreshGraph(period);
	},
	refreshGraph: function(period) {
		var deferred,source,
		_this = this;
		source=this.args.source;
		if(_this.args.source.indexOf("Graphite")>-1){
		deferred = this.getAjaxData(period);
		return deferred.done(function(result) {
			var annotations, el, i, result_data, series, _i, _len;
			if (result.length <= 0) {
				return;
			}
			result_data = _.filter(result, function(el) {
				var _ref;
				return el.target !== ((_ref = _this.args.annotator_target) != null ? _ref.replace(/["']/g, '') : void 0);
			});
			result_data = _this.preProcess(result_data);   
			if (!_this.graph) {
				_this.success(_this.parseGraphiteData(result_data, _this.args.null_as));
			}
			series = _this.parseGraphiteData(result_data, _this.args.null_as);
			if (_this.args.annotator_target) {
				annotations = _this.parseGraphiteData(_.filter(result, function(el) {
					return el.target === _this.args.annotator_target.replace(/["']/g, '');
				}), _this.args.null_as);
			}
			for (i = _i = 0, _len = series.length; _i < _len; i = ++_i) {
				el = series[i];
				_this.graph.series[i].data = el.data;
				_this.addTotals(i);
			}
			_this.graph.renderer.unstack = _this.args.unstack;
			_this.graph.render();
			if (_this.args.events) {
				deferred = _this.getEvents(period);
				deferred.done(function(result) {
					return _this.addEventAnnotations(result);
				});
			}
			_this.addAnnotations(annotations, _this.args.annotator_description);
			return _this.args.onRefresh(_this);
		});
	}
	else{//this is added for kpoint db
		var deferred;
		if(this.args.targets.length){
			deferred=kpointAjaxData(this.args.targets);
			deferred.done(function(result) {
				var annotations, el, i, result_data, series, _i, _len;
				kpoint=JSON.parse(result);
				var obj=createGraphiteLike(kpoint,_this.args.targets);
				result_data = _.filter(obj, function(el) {
					var _ref;
					return el.target !== ((_ref = _this.args.annotator_target) != null ? _ref.replace(/["']/g, '') : void 0);
				});
				result_data = _this.preProcess(result_data);
				if (!_this.graph) {
					_this.success(_this.parseGraphiteData(result_data, _this.args.null_as));
				}
				series = _this.parseGraphiteData(result_data, _this.args.null_as);
				for (i = _i = 0, _len = series.length; _i < _len; i = ++_i) {
					el = series[i];
					_this.graph.series[i].data = el.data;
					_this.addTotals(i);
				}
				_this.graph.renderer.unstack = _this.args.unstack;
				_this.graph.render();
			});
		}
	}
},

addTotals: function(i) {
    var avg, label, max, min, series_data, sum, totals;
    label = $(this.legend.lines[i].element).find('span.label').text();
    $(this.legend.lines[i].element).find('span.totals').remove();
    series_data = _.map(this.legend.lines[i].series.data, function(d) {
      return d.y;
    });
    sum = this.args.totals_formatter(_sum(series_data));
    max = this.args.totals_formatter(_max(series_data));
    min = this.args.totals_formatter(_min(series_data));
    avg = this.args.totals_formatter(_avg(series_data));
    totals = "<span class='totals pull-right'>";
    if (__indexOf.call(this.args.totals_fields, "sum") >= 0) {
      totals = totals + (" &Sigma;: " + sum);
    }
    if (__indexOf.call(this.args.totals_fields, "min") >= 0) {
      totals = totals + (" <i class='icon-caret-down'></i>: " + min);
    }
    if (__indexOf.call(this.args.totals_fields, "max") >= 0) {
      totals = totals + (" <i class='icon-caret-up'></i>: " + max);
    }
    if (__indexOf.call(this.args.totals_fields, "avg") >= 0) {
      totals = totals + (" <i class='icon-sort'></i>: " + avg);
    }
    totals += "</span>";
    return $(this.legend.lines[i].element).append(totals);
  },

preProcess: function(result) {
	var item, _i, _len;
	for (_i = 0, _len = result.length; _i < _len; _i++) {
		item = result[_i];
		if (item.datapoints.length === 1) {
			item.datapoints[0][1] = 0;
			if (this.args.unstack) {
				item.datapoints.push([0, 1]);
			} else {
				item.datapoints.push([item.datapoints[0][0], 1]);
			}
		}
	}
	return result;
},

parseGraphiteData: function(d, null_as) {
	var palette, rev_xy, targets;
	if (null_as == null) {
		null_as = null;
	}
	rev_xy = function(datapoints) {
		return _.map(datapoints, function(point) {
			return {
				'x': point[1],
				'y': point[0] !== null ? point[0] : null_as
			};
		});
	};
	palette = new Rickshaw.Color.Palette({
		scheme: this.args.scheme
	});
	targets = this.args.target || this.args.targets;
	d = _.map(d, function(el) {
		var color, _ref;
		if ((_ref = typeof targets) === "string" || _ref === "function") {
			color = palette.color();
		} else {
			color = getTargetColor(targets, el.target) || palette.color();
		}
		return {
			"color": color,
			"name": el.target,
			"data": rev_xy(el.datapoints)
		};
	});
	Rickshaw.Series.zeroFill(d);
	return d;
},
addEventAnnotations: function(events_json) {
	var active_annotation, event, _i, _len, _ref, _ref1;
	if (!events_json) {
		return;
	}
	this.annotator || (this.annotator = new GiraffeAnnotate({
	graph: this.graph,
	element: $("" + this.args.anchor + " .timeline")[0]
	}));
	this.annotator.data = {};
	$(this.annotator.elements.timeline).empty();
	active_annotation = $(this.annotator.elements.timeline).parent().find('.annotation_line.active').size() > 0;
	if ((_ref = $(this.annotator.elements.timeline).parent()) != null) {
		_ref.find('.annotation_line').remove();
	}
	for (_i = 0, _len = events_json.length; _i < _len; _i++) {
		event = events_json[_i];
		this.annotator.add(event.when, "" + event.what + " " + (event.data || ''));
	}
	this.annotator.update();
	if (active_annotation) {
		return (_ref1 = $(this.annotator.elements.timeline).parent()) != null ? _ref1.find('.annotation_line').addClass	('active') : void 0;
	}
},
addAnnotations: function(annotations, description) {
	var annotation_timestamps, _ref;
	if (!annotations) {
		return;
	}
	annotation_timestamps = _((_ref = annotations[0]) != null ? _ref.data : void 0).filter(function(el) {
		return el.y !== 0 && el.y !== null;
	});
	return this.addEventAnnotations(_.map(annotation_timestamps, function(a) {
		return {	
			when: a.x,
			what: description
		};
	}));
},
getEvents: function(period) {
	var deferred,
	_this = this;
	this.period = period;
	return deferred = $.ajax({
		dataType: 'json',
		url: generateEventsURL(this.args.events),
		error: function(xhr, textStatus, errorThrown) {
			if (textStatus === 'parsererror' && /was not called/.test(errorThrown.message)) {
				window.json_fallback = true;
				return _this.refreshGraph(period);
			} else {
				return console.log("error loading eventsURL: " + generateEventsURL(_this.args.events));
			}
		}
	});
},
getAjaxData: function(period) {	
	var deferred;
	this.period = period;
	return deferred = $.ajax({
		dataType: 'json',
		url: generateDataURL(this.args.targets, this.args.annotator_target, this.args.width),
		error: this.error.bind(this)
	});
}
});
}catch(e){
	console.error(e);
}


$('.timepanel').on('click', 'a.range', function() {
	var dash, timeFrame, _ref;
	period = $(this).attr('data-timeframe') || default_period;
	dataPoll();
	timeFrame = $(this).attr('href').replace(/^#/, '');
	dash = (_ref = $.bbq.getState()) != null ? _ref.dashboard : void 0;
	$.bbq.pushState({
		timeFrame: timeFrame,
		dashboard: dash || dashboard.name
	});
	$(this).parent('.btn-group').find('a').removeClass('active');
	$(this).addClass('active');
	return false;
});

$('#legend-toggle').on('click', function() {
	$(this).toggleClass('active');
	$('.legend').toggle();
	return false;
});

$(window).bind('hashchange', function(e) {
	var pattern = new RegExp("graph");
	if(pattern.test(e.fragment))
		return;
	var dash, timeFrame, _ref, _ref1;
	timeFrame = ((_ref = e.getState()) != null ? _ref.timeFrame : void 0) || $(".timepanel a.range[data-timeframe='" + default_period + "']")[0].text || "1d";
	return $('.timepanel a.range[href="#' + timeFrame + '"]').click();
});

$(function() {
	$(window).trigger('hashchange');
	return init();
});
